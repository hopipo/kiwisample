package com.example.kiwitest.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.example.kiwitest.model.Flight
import com.example.kiwitest.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class FlightsViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: Repository = Repository(application)
    private val todaysFlights: LiveData<List<Flight>>
    val flightsForToday: MutableLiveData<List<Flight>> = MutableLiveData()

    private val today: String

    init {
        val formatter = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        today = formatter.format(Date())

        todaysFlights = repository.getTodaysFlightsFromDatabase(today)
        viewModelScope.launch {
            todaysFlights.asFlow().collect {
                if (it.isNullOrEmpty()) {
                    getUniqueFlights()
                } else {
                    flightsForToday.postValue(todaysFlights.value)
                }
            }
        }
    }

    private fun getUniqueFlights() {
        viewModelScope.launch(Dispatchers.IO) {
            val remoteFlightsList = repository.getFlights().flights

            val remoteFlightsListId = remoteFlightsList.map { it.id }
            val listedFlightsListId = repository.getAllFlightsFromDatabase().map { it.id }
            val differenceFlightsListId: List<String> = remoteFlightsListId.minus(listedFlightsListId).take(DAILY_FLIGHTS_LIMIT)

            val difference: List<Flight> = if (differenceFlightsListId.isEmpty()) {
                repository.deleteAllFlightsFromDatabase()
                remoteFlightsList.take(DAILY_FLIGHTS_LIMIT)
            } else {
                remoteFlightsList.filter { flight -> differenceFlightsListId.any { id -> flight.id == id } }
            }

            flightsForToday.postValue(difference)
            repository.addFlights(difference, today)
        }
    }

    companion object {
        const val DATE_FORMAT = "dd-MM-yyyy"
        const val DAILY_FLIGHTS_LIMIT = 5
    }
}