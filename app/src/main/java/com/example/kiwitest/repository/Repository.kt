package com.example.kiwitest.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.kiwitest.api.RetrofitInstance
import com.example.kiwitest.data.FlightDatabase
import com.example.kiwitest.model.Flight
import com.example.kiwitest.model.Flights

class Repository(application: Application) {

    private val flightDao = FlightDatabase.getDatabase(application).flightDao()

    suspend fun addFlights(flights: List<Flight>, today: String) {
        flights.forEach { flight ->
            flight.listedOnDate = today
            flightDao.addFlight(flight)
        }
    }

    suspend fun getFlights(): Flights {
        return RetrofitInstance.api.getFlights()
    }

    fun getTodaysFlightsFromDatabase(date: String): LiveData<List<Flight>> {
        return flightDao.getTodaysFlights(date)
    }

    fun getAllFlightsFromDatabase(): List<Flight> {
        return flightDao.getAllFlights()
    }

    suspend fun deleteAllFlightsFromDatabase() {
        flightDao.deleteAllFlights()
    }

}