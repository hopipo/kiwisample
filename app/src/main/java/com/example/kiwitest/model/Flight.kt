package com.example.kiwitest.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import kotlinx.parcelize.Parcelize

data class Flights(
    @SerializedName("data") val flights: List<Flight>
)

@Parcelize
@Entity(tableName = "flight_table")
data class Flight(
    @PrimaryKey @SerializedName("id") val id: String,
    @SerializedName("dTime") val departureTime: Long,
    @SerializedName("aTime") val arrivalTime: Long,
    @SerializedName("cityFrom") val cityFrom: String,
    @SerializedName("cityTo") val cityTo: String,
    @SerializedName("mapIdto") val destination: String,
    @SerializedName("price") val price: Float,
    @SerializedName("baglimit") val bagData: BagData,
    @SerializedName("technical_stops") val stops: Int,
    var listedOnDate: String?,
) : Parcelable

@Parcelize
data class BagData(
    @SerializedName("hand_weight") val handWeight: Int,
    @SerializedName("hold_weight") val holdWeight: Int
) : Parcelable


class Converters {

    @TypeConverter
    fun fromBagData(bagData: BagData): String {
        return Gson().toJson(bagData)
    }

    @TypeConverter
    fun toBagData(bagDataJson: String): BagData {
        return Gson().fromJson(bagDataJson, object : TypeToken<BagData>() {}.type)
    }
}