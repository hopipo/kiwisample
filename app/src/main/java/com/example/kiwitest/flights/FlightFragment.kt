package com.example.kiwitest.flights

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.kiwitest.databinding.FragmentFlightBinding
import com.example.kiwitest.model.BagData
import com.example.kiwitest.model.Flight
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class FlightFragment() : Fragment() {

    private var fragmentBinding: FragmentFlightBinding? = null
    private var bitmap: MutableLiveData<Bitmap> = MutableLiveData()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentFlightBinding.inflate(inflater, container, false)
        fragmentBinding = binding

        arguments?.takeIf {
            it.containsKey(FLIGHT_DATA)
        }?.apply {
            val data = this.getParcelable<Flight>(FLIGHT_DATA)

            getFutureData(data?.destination)
            bitmap.observe(viewLifecycleOwner, { bitmap ->
                binding.destinationImageView.setImageBitmap(bitmap)
            })

            binding.destinationTitle.text = data?.cityTo
            binding.departureDate.text =
                data?.departureTime?.let { getFormattedDate(DATE_FORMAT, it) }
            binding.arrivalDate.text = data?.arrivalTime?.let { getFormattedDate(DATE_FORMAT, it) }

            binding.departureTime.text =
                data?.departureTime?.let { getFormattedDate(TIME_FORMAT, it) }
            binding.departurePlace.text = data?.cityFrom
            binding.arrivalTime.text = data?.arrivalTime?.let { getFormattedDate(TIME_FORMAT, it) }
            binding.arrivalPlace.text = data?.cityTo

            val priceString = "${data?.price.toString()} €"
            binding.price.text = priceString
            binding.chipStops.text = data?.stops.toString()
            binding.chipWeight.text = data?.bagData?.let { getCalculatedWeight(it) }
        }

        return binding.root;
    }

    override fun onDestroyView() {
        fragmentBinding = null
        super.onDestroyView()
    }

    private fun getFutureData(destinationName: String?) {
        lifecycleScope.launch(Dispatchers.IO) {
            bitmap.postValue(
                Glide.with(requireContext())
                    .asBitmap()
                    .load("$IMAGES_URL$destinationName.jpg")
                    .submit(600, 330).get()
            )
        }
    }

    private fun getFormattedDate(format: String, timeInSeconds: Long): String {
        val formatter = SimpleDateFormat(format, Locale.getDefault())
        return formatter.format(TimeUnit.SECONDS.toMillis(timeInSeconds))
    }

    private fun getCalculatedWeight(bagData: BagData): String {
        val weight: Int = bagData.handWeight + bagData.holdWeight
        return "$weight kg"
    }

    companion object {
        const val TIME_FORMAT = "HH:mm"
        const val DATE_FORMAT = "EEE dd. MM."
        const val FLIGHT_DATA = "flight_data"
        const val IMAGES_URL = "https://images.kiwi.com/photos/600x330/"
    }

}