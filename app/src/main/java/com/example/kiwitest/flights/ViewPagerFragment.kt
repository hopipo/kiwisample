package com.example.kiwitest.flights

import android.gesture.GestureOverlayView.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.kiwitest.R
import com.example.kiwitest.databinding.FragmentViewPagerBinding
import com.example.kiwitest.viewModel.FlightsViewModel
import com.google.android.material.tabs.TabLayoutMediator

class ViewPagerFragment : Fragment() {

    private var fragmentBinding: FragmentViewPagerBinding? = null
    private lateinit var mFlightsViewModel: FlightsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        fragmentBinding = binding

        setupViewPager()

        val adapter = ViewPagerAdapter(requireActivity().supportFragmentManager, lifecycle)
        binding.flightViewPager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.flightViewPager) { tab, position ->
        }.attach()

        this.mFlightsViewModel = ViewModelProvider(this).get(FlightsViewModel::class.java)
        mFlightsViewModel.flightsForToday.observe(viewLifecycleOwner, Observer { flightList ->
            adapter.setData(flightList)
            binding.progressbar.visibility = GONE
            binding.flightViewPager.visibility = VISIBLE
        })

        return binding.root;
    }

    override fun onDestroyView() {
        fragmentBinding = null
        super.onDestroyView()
    }

    private fun setupViewPager() {
        fragmentBinding!!.flightViewPager.apply {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 4
        }

        val pageMarginPx = resources.getDimensionPixelOffset(R.dimen.pageMargin)
        val offsetPx = resources.getDimensionPixelOffset(R.dimen.offset)
        fragmentBinding!!.flightViewPager.setPageTransformer { page, position ->
            val viewPager = page.parent.parent as ViewPager2
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (viewPager.orientation == ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
        }
    }

}