package com.example.kiwitest.flights

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.kiwitest.flights.FlightFragment.Companion.FLIGHT_DATA
import com.example.kiwitest.model.Flight

class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private var flightList = emptyList<Flight>()

    override fun createFragment(position: Int): Fragment {
        val flightFragment = FlightFragment()
        flightFragment.arguments = Bundle().apply {
            val flight = flightList[position]
            putParcelable(FLIGHT_DATA, flight)
        }
        return flightFragment
    }

    override fun getItemCount(): Int {
        return flightList.size
    }

    fun setData(flightList: List<Flight>) {
        this.flightList = flightList
        notifyDataSetChanged()
    }

}