package com.example.kiwitest.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.kiwitest.model.Flight

@Dao
interface FlightDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addFlight(flight: Flight)

    @Query("SELECT * FROM flight_table WHERE listedOnDate = :date ORDER BY listedOnDate ASC")
    fun getTodaysFlights(date: String): LiveData<List<Flight>>

    @Query("SELECT * FROM flight_table ORDER BY listedOnDate ASC")
    fun getAllFlights(): List<Flight>

    @Query("DELETE FROM flight_table")
    suspend fun deleteAllFlights()
}